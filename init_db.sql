-- Adminer 4.8.1 MySQL 11.0.2-MariaDB-1:11.0.2+maria~ubu2204 dump

-- Names, Emails, Address shall be stored and compared in uppercase, EXCEPT usernames and passwords --

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

CREATE DATABASE IF NOT EXISTS `tradespace`;
USE `tradespace`;

CREATE TABLE `country_codes` (
  `country_code` varchar(4) NOT NULL,
  `country_name` varchar(24) DEFAULT NULL,
  PRIMARY KEY (`country_code`),
  UNIQUE KEY `country_name` (`country_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `country_codes` (`country_code`, `country_name`) VALUES
('ARG',	'Argentina'),
('AUS',	'Australia'),
('AUT',	'Austria'),
('BHR',	'Bahrain'),
('BRA',	'Brazil'),
('CAN',	'Canada'),
('CHL',	'Chile'),
('CHN',	'China'),
('COL',	'Colombia'),
('CRI',	'Costa Rica'),
('CUB',	'Cuba'),
('DNK',	'Denmark'),
('DOM',	'Dominican Republic'),
('EGY',	'Egypt'),
('SLV',	'El Salvador'),
('FRA',	'France'),
('DEU',	'Germany'),
('GHA',	'Ghana'),
('HKG',	'Hong Kong'),
('IND',	'India'),
('IDN',	'Indonesia'),
('IRN',	'Iran'),
('ISR',	'Israel'),
('ITA',	'Italy'),
('JPN',	'Japan'),
('JOR',	'Jordan'),
('KEN',	'Kenya'),
('KWT',	'Kuwait'),
('MYS',	'Malaysia'),
('MAR',	'Morocco'),
('NLD',	'Netherlands'),
('NZL',	'New Zealand'),
('NGA',	'Nigeria'),
('NOR',	'Norway'),
('OMN',	'Oman'),
('PAN',	'Panama'),
('PER',	'Peru'),
('PHL',	'Philippines'),
('QAT',	'Qatar'),
('RUS',	'Russia'),
('SAU',	'Saudi Arabia'),
('SEN',	'Senegal'),
('SGP',	'Singapore'),
('RSA',	'South Africa'),
('KOR',	'South Korea'),
('ESP',	'Spain'),
('SWE',	'Sweden'),
('CHE',	'Switzerland'),
('THA',	'Thailand'),
('TUR',	'Turkey'),
('ARE',	'United Arab Emirates'),
('GBR',	'United Kingdom'),
('USA',	'United States of America'),
('VNZ',	'Venezuela'),
('VNM',	'Vietnam');

CREATE TABLE `banks` (
  `bank_id` int(7) unsigned ZEROFILL AUTO_INCREMENT,
  `_name` varchar(128) NOT NULL,
  `country` varchar(24) NOT NULL,
  `_location` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`bank_id`),
  CONSTRAINT `banks_ibfk_1` FOREIGN KEY (`country`) REFERENCES `country_codes` (`country_name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `banks` (`bank_id`, `_name`, `country`) VALUES
(1078121, 'Scotiabank (Bank of Nova Scotia)', 'Canada');

INSERT INTO `banks` (`_name`, `country`) VALUES
('TD Canada Trust', 'Canada'),
('RBC (Royal Bank of Canada)', 'Canada'),
('BMO (Bank of Montreal)', 'Canada'),
('CIBC (Canadian Imperial Bank of Commerce)', 'Canada'),
('Simplii Financial', 'Canada'),
('Coast Capital Savings', 'Canada'),
('NBC (National Bank of Canada)', 'Canada'),
('Tangerine', 'Canada'),
('Vancity', 'Canada'),
('Chase Bank (JPMorgan)', 'United States of America'),
('BoA (Bank of America)', 'United States of America'),
('Citibank (Citigroup)', 'United States of America'),
('Wells Fargo', 'United States of America'),
('Goldman Sachs', 'United States of America'),
('Capital One', 'United States of America'),
('ICBC (Industrial and Commercial Bank of China)', 'China'),
('CCB (China Construction Bank)', 'China'),
('Bank of China', 'China'),
('State Bank of India', 'India'),
('Punjab National Bank', 'India'),
('Bank of India', 'India'),
('Axis Bank', 'India'),
('HDFC Bank', 'India'),
('ICIC Bank', 'India');

CREATE TABLE `currency` (
  `currency_code` char(3) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  PRIMARY KEY (`currency_code`),
  UNIQUE KEY `full_name` (`full_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `currency` (`currency_code`, `full_name`) VALUES
('AED',	'United Arab Emirates Dirham'),
('ARS',	'Argentine Peso'),
('AUD',	'Australian Dollar'),
('BHD',	'Bahraini Dinar'),
('BRL',	'Brazilian Real'),
('CAD',	'Canadian Dollar'),
('CHF',	'Swiss Franc'),
('CLP',	'Chilean Peso'),
('CNY',	'Chinese Yuan'),
('COP',	'Colombian Peso'),
('CRC',	'Costa Rican Colón'),
('CUP',	'Cuban Peso'),
('DOP',	'Dominican Peso'),
('EGP',	'Egyptian Pound'),
('EUR',	'Euro'),
('GBP',	'British Pound'),
('GHS',	'Ghanaian Cedi'),
('HKD',	'Hong Kong Dollar'),
('IDR',	'Indonesian Rupiah'),
('ILS',	'Israeli New Shekel'),
('INR',	'Indian Rupee'),
('IRR',	'Iranian Rial'),
('JOD',	'Jordanian Dinar'),
('JPY',	'Japanese Yen'),
('KES',	'Kenyan Shilling'),
('KRW',	'South Korean Won'),
('KWD',	'Kuwaiti Dinar'),
('MAD',	'Moroccan Dirham'),
('MYR',	'Malaysian Ringgit'),
('NGN',	'Nigerian Naira'),
('NZD',	'New Zealand Dollar'),
('OMR',	'Omani Rial'),
('PAB',	'Panamanian Balboa'),
('PEN',	'Peruvian Sol'),
('PHP',	'Philippine Peso'),
('QAR',	'Qatari Riyal'),
('RUB',	'Russian Ruble'),
('SAR',	'Saudi Riyal'),
('SGD',	'Singapore Dollar'),
('THB',	'Thai Baht'),
('TRY',	'Turkish Lira'),
('USD',	'United States Dollar'),
('VND',	'Vietnamese Dong'),
('ZAR',	'South African Rand');

CREATE TABLE `customers` (
  `customer_id` int(7) unsigned ZEROFILL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) NOT NULL,
  `suffix_title` enum('MR','MRS','MS','DR','MX') DEFAULT NULL,
  `dob` date NOT NULL,
  `primary_phone` varchar(32) DEFAULT NULL,
  `alt_phone` varchar(32) DEFAULT NULL,
  `email_addr` varchar(100) NOT NULL UNIQUE,
  `created_at` date DEFAULT current_timestamp(),
  PRIMARY KEY (`customer_id`),
  UNIQUE KEY (`email_addr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `customers` (`customer_id`, `first_name`, `middle_name`, `last_name`, `suffix_title`, `dob`, `primary_phone`, `alt_phone`, `email_addr`, `created_at`) VALUES
(4151358,	'BECK',	'S',	'RICKERT',	'MR',	'1998-04-24',	'16045882545',	'17789036628',	'beck@beckrickert.com',	DEFAULT),
(4179141,	'GABRIELA',	DEFAULT,	'RENDEROS',	'MS',	'1998-06-21',	'17786624181',	'12368184619',	'beckhdtm@hotmail.com',	DEFAULT);

CREATE TABLE `customer_employment` (
  `cust_emp_id` int(7) unsigned ZEROFILL AUTO_INCREMENT,
  `customer_id` int(7) unsigned NOT NULL,
  `emp_addr_full` varchar(128) NULL DEFAULT NULL,
  `emp_phone` varchar(32) NULL DEFAULT NULL,
  `company_name` varchar(64) NOT NULL,
  `industry` varchar(32) NULL DEFAULT NULL,
  `_position_name` varchar(32) NULL DEFAULT NULL,
  PRIMARY KEY (`cust_emp_id`),
  UNIQUE KEY `customer_id` (`customer_id`),
  CONSTRAINT `customer_employment_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `customer_employment` (`cust_emp_id`, `customer_id`, `emp_addr_full`, `emp_phone`, `company_name`, `industry`, `_position_name`) VALUES
(1034173, 4151358, "101-123 Sesame St, Manhattan, New York, 98230, USA", '13605555151', 'Sesame Street Inc', 'Logistics', 'CEO'),
(1034174, 4179141, "365 D St, Blaine, WA, 98230, USA", '13605555151', 'Chevron', 'Retail', 'Store Manager');

CREATE TABLE `customer_identity` (
  `identity_id` int(7) unsigned ZEROFILL AUTO_INCREMENT,
  `customer_id` int(7) unsigned NOT NULL,
  `identity_type` int(7) unsigned NOT NULL,
  `_number` varchar(24) NOT NULL,
  `expiry_issue_date` date DEFAULT NULL,
  `added_at` date DEFAULT current_timestamp(),
  PRIMARY KEY (`identity_id`),
  KEY `identity_type` (`identity_type`),
  CONSTRAINT `customer_identity_ibfk_1` FOREIGN KEY (`identity_type`) REFERENCES `identities` (`identity_type`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `customer_identity_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `customer_identity` (`identity_id`, `customer_id`, `identity_type`, `_number`, `expiry_issue_date`, `added_at`) VALUES
(1728074, 4151358, 1186329, "GC710182", 2028-06-24, DEFAULT),
(1728075, 4179141, 1186329, "AB891121", 2024-05-12, DEFAULT);

CREATE TABLE `identities` (
  `identity_type` int(7) unsigned ZEROFILL AUTO_INCREMENT,
  `_name` varchar(32) NOT NULL,
  `issued_country_code` varchar(4) NOT NULL,
  `issued_territory` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`identity_type`),
  KEY `issued_country_code` (`issued_country_code`),
  CONSTRAINT `identities_ibfk_1` FOREIGN KEY (`issued_country_code`) REFERENCES `country_codes` (`country_code`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `identities` (`identity_type`, `_name`, `issued_country_code`, `issued_territory`) VALUES
(1186329, 'Canadian Passport', 'CAN', NULL);

INSERT INTO `identities` (`_name`, `issued_country_code`, `issued_territory`) VALUES
('Provincial Services Card', 'CAN', 'BRITISH COLUMBIA'),
('Provincial Services Card', 'CAN', 'ALBERTA'),
('Provincial Services Card', 'CAN', 'ONTARIO'),
('Drivers License', 'CAN', 'BRITISH COLUMBIA'),
('Permanent Resident Card', 'CAN', NULL),
('PR Card (Green Card)', 'USA', NULL),
('USA Passport', 'USA', NULL),
('Nexus Card', 'USA', NULL),
('Nexus Card', 'CAN', NULL),
('IMM 1442 Study Permit', 'CAN', NULL),
('IMM 1442 Work Permit', 'CAN', NULL),
('State-issued ID', 'USA', 'WASHINGTON'),
('State-issued ID', 'USA', 'NEW YORK');

CREATE TABLE `personal_addresses` (
  `addr_id` int(7) unsigned ZEROFILL AUTO_INCREMENT,
  `customer_id` int(7) unsigned NOT NULL,
  `address_line_1` varchar(64) DEFAULT NULL,
  `address_line_2` varchar(64) DEFAULT NULL,
  `city` varchar(64) DEFAULT NULL,
  `_locale` varchar(64) DEFAULT NULL,
  `postal_code` varchar(12) DEFAULT NULL,
  `country` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`addr_id`),
  UNIQUE KEY `customer_id` (`customer_id`),
  CONSTRAINT `personal_addresses_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `personal_addresses` (`addr_id`, `customer_id`, `address_line_1`, `city`, `_locale`, `postal_code`, `country`) VALUES
(1571033, 4151358, '12666 72 Ave', 'Surrey', 'BC', 'V3W2M8', 'Canada'),
(1571034, 4179141, '7011 49 W St', 'Vancouver', 'BC', 'V4N7H1', 'Canada');

CREATE TABLE `sourcing_accounts` (
  `source_id` int(7) unsigned ZEROFILL AUTO_INCREMENT,
  `acct_num` varchar(18) NOT NULL,
  `customer_id` int(7) unsigned NOT NULL,
  `currency_code` char(3) NOT NULL,
  `routing_code` varchar(16) NOT NULL,
  `bank_id` int(7) unsigned NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`source_id`),
  KEY `customer_id` (`customer_id`),
  KEY `currency_code` (`currency_code`),
  KEY `bank_id` (`bank_id`),
  CONSTRAINT `sourcing_accounts_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sourcing_accounts_ibfk_2` FOREIGN KEY (`currency_code`) REFERENCES `currency` (`currency_code`) ON UPDATE CASCADE,
  CONSTRAINT `sourcing_accounts_ibfk_3` FOREIGN KEY (`bank_id`) REFERENCES `banks` (`bank_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `sourcing_accounts` (`source_id`, `acct_num`, `customer_id`, `currency_code`, `routing_code`, `bank_id`, `date_added`) VALUES
(128180, '120700727851', 4151358, 'CAD', '002', 1078121, DEFAULT);

CREATE TABLE `tradespace_accounts` (
  `acct_num` varchar(18) NOT NULL,
  `customer_id` int(7) unsigned NOT NULL,
  `total_bal` decimal(15,2) NOT NULL DEFAULT 0.00,
  `avail_bal` decimal(15,2) NOT NULL DEFAULT 0.00,
  `currency_code` char(3) NOT NULL,
  `date_open` date NOT NULL DEFAULT current_timestamp(),
  `_status` enum('OPEN','CLOSED','FROZEN') DEFAULT 'OPEN',
  PRIMARY KEY (`acct_num`),
  KEY `customer_id` (`customer_id`),
  KEY `currency_code` (`currency_code`),
  CONSTRAINT `tradespace_accounts_ibfk_3` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tradespace_accounts_ibfk_4` FOREIGN KEY (`currency_code`) REFERENCES `currency` (`currency_code`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `tradespace_accounts` (`acct_num`, `customer_id`, `total_bal`, `avail_bal`, `currency_code`, `date_open`, `_status`) VALUES
('312000412720804221', 4151358, 2371.85, 1370.85,	'CAD', DEFAULT, DEFAULT),
('312001523831915332', 4179141, 1509.89, 801.05, 'USD', DEFAULT, DEFAULT);

CREATE TABLE `tradespace_recipients` (
  `r_id` int(7) unsigned ZEROFILL AUTO_INCREMENT,
  `owner_id` int(7) unsigned NOT NULL,
  `recipient_id` int(7) unsigned NOT NULL,
  `dest_acct` varchar(18) NOT NULL,
  `email_addr` varchar(100) NOT NULL,
  `identity_index` int(7) unsigned DEFAULT NULL,
  `identity_num` varchar(24) DEFAULT NULL,
  `identity_issue_expiry_date` date DEFAULT NULL,
  PRIMARY KEY (`r_id`),
  KEY `owner_id` (`owner_id`),
  KEY `recipient_id` (`recipient_id`),
  KEY `dest_acct` (`dest_acct`),
  KEY `identity_index` (`identity_index`),
  CONSTRAINT `tradespace_recipients_ibfk_5` FOREIGN KEY (`owner_id`) REFERENCES `customers` (`customer_id`) ON UPDATE CASCADE,
  CONSTRAINT `tradespace_recipients_ibfk_6` FOREIGN KEY (`recipient_id`) REFERENCES `customers` (`customer_id`) ON UPDATE CASCADE,
  CONSTRAINT `tradespace_recipients_ibfk_7` FOREIGN KEY (`dest_acct`) REFERENCES `tradespace_accounts` (`acct_num`) ON UPDATE CASCADE,
  CONSTRAINT `tradespace_recipients_ibfk_8` FOREIGN KEY (`identity_index`) REFERENCES `identities` (`identity_type`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `tradespace_recipients` (`r_id`, `owner_id`, `recipient_id`, `dest_acct`, `email_addr`, `identity_index`, `identity_num`, `identity_issue_expiry_date`) VALUES
(1942544, 4151358, 4179141, '312001523831915332', 'beckhdtm@hotmail.com', 1186329, 'OT474112', 2029-11-12);

CREATE TABLE `tradespace_transactions` (
  `transaction_id` int(7) unsigned ZEROFILL AUTO_INCREMENT,
  `transfer_id` int(7) unsigned DEFAULT NULL,
  `acct_num` varchar(18) NOT NULL,
  `trans_amount` decimal(15,2) NOT NULL,
  `trans_type` enum('DR','CR','CORR','RTN','SC','MISC','OTH') DEFAULT 'MISC',
  `posted_date` date NOT NULL DEFAULT current_timestamp(),
  `_desc` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`transaction_id`),
  KEY `acct_num` (`acct_num`),
  KEY `transfer_id` (`transfer_id`),
  CONSTRAINT `tradespace_transactions_ibfk_1` FOREIGN KEY (`acct_num`) REFERENCES `tradespace_accounts` (`acct_num`) ON UPDATE CASCADE,
  CONSTRAINT `tradespace_transactions_ibfk_2` FOREIGN KEY (`transfer_id`) REFERENCES `tradespace_transfers` (`transfer_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci AUTO_INCREMENT=2491012;

CREATE TABLE `tradespace_transfers` (
  `transfer_id` int(7) unsigned ZEROFILL AUTO_INCREMENT,
  `origin_acct` varchar(18) NOT NULL,
  `dest_acct` varchar(18) NOT NULL,
  `recip_index` int(7) unsigned NOT NULL,
  `origin_currency` char(3) NOT NULL,
  `dest_currency` char(3) NOT NULL,
  `fx_rate` decimal(15,6) NOT NULL DEFAULT 1.000000,
  `amount` decimal(15,2) NOT NULL,
  `init_time` datetime DEFAULT current_timestamp(),
  `secret_hash` char(72) NOT NULL,
  `memo` varchar(256) DEFAULT NULL,
  `_status` enum('PENDING','COMPLETE','RETURNED','EXPIRED') DEFAULT 'PENDING',
  PRIMARY KEY (`transfer_id`),
  KEY `origin_acct` (`origin_acct`),
  KEY `dest_acct` (`dest_acct`),
  KEY `recip_index` (`recip_index`),
  KEY `origin_currency` (`origin_currency`),
  KEY `dest_currency` (`dest_currency`),
  CONSTRAINT `tradespace_transfers_ibfk_1` FOREIGN KEY (`origin_acct`) REFERENCES `tradespace_accounts` (`acct_num`),
  CONSTRAINT `tradespace_transfers_ibfk_2` FOREIGN KEY (`dest_acct`) REFERENCES `tradespace_accounts` (`acct_num`),
  CONSTRAINT `tradespace_transfers_ibfk_3` FOREIGN KEY (`recip_index`) REFERENCES `tradespace_recipients` (`r_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tradespace_transfers_ibfk_4` FOREIGN KEY (`origin_currency`) REFERENCES `currency` (`currency_code`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `tradespace_transfers_ibfk_5` FOREIGN KEY (`dest_currency`) REFERENCES `currency` (`currency_code`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci AUTO_INCREMENT=3589077;

CREATE TABLE `user_auth` (
  `customer_id` int(7) unsigned NOT NULL,
  `username` varchar(32) UNIQUE NOT NULL,
  `passwd_hash` char(60) NOT NULL,
  `otp_key` varchar(64) NULL,
  `_2fa_type` enum('SMS','EMAIL','APP') NOT NULL DEFAULT 'EMAIL',
  `recovery_key` char(64) UNIQUE DEFAULT NULL, 
  PRIMARY KEY (`customer_id`),
  UNIQUE KEY (`username`),
  CONSTRAINT `user_auth_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `user_auth` (`customer_id`, `username`, `passwd_hash`, `otp_key`, `_2fa_type`) VALUES
(4151358,	'brickert',	'$2a$10$HrMlryzTGfJzbREXRnjan.aMsgW7IEwX73nps11ISaMNZLC/HGLYO',	NULL,	DEFAULT);


CREATE USER 'tradespace_user'@'%' IDENTIFIED BY 'tradespace1234';
GRANT ALL PRIVILEGES ON `tradespace`.* TO `tradespace_user`@`%`;
FLUSH PRIVILEGES;